# odv-www

This repository hosts the latest version of the documentation for the [Open Data Visualization Markup Language](https://eightsquaredsoftware.com/odv/), a human-editable and machine-readable format for describing certain classes of data visualizations in a platform- and device-independent manner.

ODVML is currently a prototype format in an early stage of development. Feedback on real-world use cases is necessary for ODVML to mature into a format deserving of the 1.0 label.

The `master` branch of this repository is automatically mirrored to a subdirectory under the [Eight Squared Software web site](https://eightsquaredsoftware.com).

## Implementations

Below are the known implementations of ODVML rasterizers. If you would like to add your implementation to the list, please submit a [merge request](merge_requests/new).

### JavaScript (browser)

- [odv-js](https://gitlab.com/eightsquared/odv-js), maintained by Eight Squared Software.

## Contributions

If you would like to propose a new feature or report an issue with the documentation, by all means, please [create an issue](issues/new).
